"""Yggdrasil
"""

from setuptools import setup, find_packages

setup(
    name='yggdrasil',
    version='0.1a1',
    description="A scriptable hierarchic remote execution engine.",
    author="Thomas Lavocat",
    author_email="tlavocat@april.org",
    url='https://gitlab.com/lavocat/yggdrasil/',
    license='GPL3',

    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
    ],

    keywords='taktuk parallel fork InSitu',

    packages=find_packages(),
    setup_requires=['pytest-runner'],
    install_requires=['pytest', 'pyzmq', 'pexpect'],

    entry_points={
        'console_scripts': [
            'erebor=yggdrasil.erebor.main:main',
            'taktuk-bridge=yggdrasil.isengard.socket_bridge:main',
            ],
        }
    )
