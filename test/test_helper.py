import pytest
from yggdrasil import Helper

def test_helper():
    helper = Helper()
    #print("node1")
    assert(" ".join(helper.build_list("node1")) == "node1")
    #print("\nnode[19]")
    assert(" ".join(helper.build_list("node[19]")) == "node19")
    #print("\nnode[1-3]")
    assert(" ".join(helper.build_list("node[1-3]")) == "node1 node2 node3")
    #print("\nnode[1-3],otherhost/node2")
    assert(" ".join(helper.build_list("node[1-3],otherhost/node2")) == "node1 node3 otherhost")
    #print("\nnode[1-3,5]part[a-b]/node[3-5]parta,node1partb")
    assert(" ".join(helper.build_list("node[1-3,5]part[a-b]/node[3-5]parta,node1partb")) == "node1parta node2parta node2partb node3partb node5partb")
    #print("\nA,B,C,D,E,F")
    assert(" ".join(helper.build_list("A,B,C,D,E,F")) == "A B C D E F")

    a = (helper.compose([1]))
    assert(a[0] == "1")
    b = (helper.compose([1,2,3,4]))
    assert(b[0] == "1-4")
    c = (helper.compose([1,4,5,10]))
    assert(c[0] == "1")
    assert(c[1] == "4-5")
    assert(c[2] == "10")
    d = (helper.compose([1,4,5,6,10,11,12]))
    assert(d[0] == "1")
    assert(d[1] == "4-6")
    assert(d[2] == "10-12")

    assert(helper.build_list("["+",".join(a)+"]") == ["1"])
    assert(helper.build_list("["+",".join(b)+"]") == ["1","2","3","4"])
    assert(helper.build_list("["+",".join(c)+"]") == ["1","4","5","10"])
    assert(helper.build_list("["+",".join(d)+"]") == ["1","4","5","6","10","11","12"])
