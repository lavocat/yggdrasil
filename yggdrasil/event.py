class Event:
    def __init__(self, f, data):
        self.function = f
        self.data     = data

    def execute(self):
        self.function(self.data)

    def __str__(self):
        return "[{} data {}]".format(self.function, self.data)
