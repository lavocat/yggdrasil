import logging


def configure_logger(logger, log, loglv, logf):
    FMT = '%(asctime)s - %(name)s - %(module)s - %(levelname)s - %(message)s'
    formatter = logging.Formatter(FMT)
    if logf:
        fh = logging.FileHandler(logf)
        fh.setFormatter(formatter)
        logger.addHandler(fh)
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    logger.setLevel(loglv)
    return logger
