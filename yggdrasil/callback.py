class CallbackObject:
    def __init__(self, dnode, dnetid, snode, snetid, callback, value):
        self.value = value
        self.dnode = dnode
        self.snode = snode
        self.dnetid = dnetid
        self.snetid = snetid
        self.callback = callback

    def fire(self, data):
        self.callback(data, self.dnode, self.dnetid, self.snode, self.snetid,
                      self.value)

    def copy_with_another_callback(self, callback):
        return CallbackObject(self.dnode, self.dnetid, self.snode,
                              self.dnetid, callback, self.value)

    def __str__(self):
        return (" TO WARN {} {} {} {} {} ".format(self.snode, self.snetid,
                                                  self.dnode, self.dnetid,
                                                  self.value))
