import sys
import struct
from .. import consts

class MPIDecoder:

    def unpack(self, to_unpack) :
        # length of the global message
        offset = 0
        fields = []
        while offset < len(to_unpack) :
            field_size  = struct.unpack_from('>I', to_unpack[offset:offset+4])[0]
            offset     += 4
            field       = to_unpack[offset:offset+field_size]
            offset     += field_size
            fields.append(field.decode(consts.encoding))
        return fields

    def pack(self, *fields) :
        packed  = None
        for field in fields :
            if packed == None :
                packed = struct.pack('>I', len(field))
            else :
                packed += struct.pack('>I', len(field))
            packed += bytes(field, consts.encoding)
        return packed;
